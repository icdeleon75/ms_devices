package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import model.Device;
import service.DeviceService;

@RestController
@CrossOrigin(origins ="*")
public class DevicesController {

	@Autowired
	DeviceService service;
	
	@GetMapping(value="/devices", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Device> getDevices(){
		return service.getDevices();
	}
	
	@GetMapping(value="/devices/{id}", produces=MediaType.APPLICATION_JSON_VALUE)
	public Device getDevice(@PathVariable("id") int deviceID) {
		return service.getDevice(deviceID);
	}
	
	@DeleteMapping(value="/devices/{id}", produces=MediaType.TEXT_PLAIN_VALUE)
	public void deleteDevice(@PathVariable("id") int id) {
		service.deleteDevice(id);
	}
	
	@PostMapping(value="devices", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.TEXT_PLAIN_VALUE)
	public void addDevice(@RequestBody Device device) {
		service.addDevice(device);
	}
	
	
	@PutMapping(value="devices", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.TEXT_PLAIN_VALUE)
	public void updateDevice(@RequestBody Device device) {
		service.updateDevice(device);
	}
	
	
}
