package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import model.DeviceType;
import service.DeviceTypeService;

@CrossOrigin(origins="*")
@RestController
public class DeviceTypesController {

	@Autowired
	DeviceTypeService service;
	
	@PostMapping(value="device-type", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.TEXT_PLAIN_VALUE)
	public boolean addDeviceType(DeviceType deviceType) {
		return service.addDeviceType(deviceType);
	}
	
	@DeleteMapping(value="device-type/{deviceTypeId}", produces=MediaType.TEXT_PLAIN_VALUE)
	public boolean deleteDeviceType(@PathVariable("deviceTypeId") int deviceTypeId) {
		return service.deleteDeviceType(deviceTypeId);
	}
	
	@GetMapping(value="device-type/{id}", produces=MediaType.APPLICATION_JSON_VALUE)
	public DeviceType getDeviceType(@PathVariable("id") int deviceTypeId)
	{
		return service.getDeviceType(deviceTypeId);
	}
}
