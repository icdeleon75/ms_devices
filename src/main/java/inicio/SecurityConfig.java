package inicio;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService());
	}
	
	@Bean
	@Override
	public UserDetailsService userDetailsService() {
	    
	    
		 @SuppressWarnings("deprecation")
			UserDetails user =
		            User.withDefaultPasswordEncoder()
		                    .username("user")
		                    .password("user")
		                    .roles("USER")
		                    .build();
	    
	    @SuppressWarnings("deprecation")
		UserDetails admin =
	            User.withDefaultPasswordEncoder()
	                    .username("admin")
	                    .password("admin")
	                    .roles("ADMIN")
	                    .build();

	    List<UserDetails> userList = new ArrayList<UserDetails>();
	    userList.add(user);
	    userList.add(admin);
	    
	    return new InMemoryUserDetailsManager(userList);
	}
	
	//Security politics definition
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
		.authorizeRequests()
		//Only the users with role ADMIN and authenticated can consume the following url and must be authenticated
		.antMatchers(HttpMethod.GET, "device-type/**").hasRole("ADMIN")
		.antMatchers("device-type/**")
		.authenticated()
		.and()
		.authorizeRequests().antMatchers(HttpMethod.GET, "device-type/**").hasRole("USER")
		.antMatchers("device-type/**").authenticated()
		.and()		
		.httpBasic();
	}
}
