package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.DeviceDao;
import model.Device;

@Service
public class DeviceServiceImpl implements DeviceService {

	@Autowired
	DeviceDao dao;
	
	@Override
	public boolean addDevice(Device device) {
		if(dao.getDevice(device.getDeviceId())==null) {
			dao.addDevice(device);
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteDevice(int deviceId) {
		if(dao.getDevice(deviceId)!=null) {
			dao.deleteDevice(deviceId);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateDevice(Device device) {
		if(dao.getDevice(device.getDeviceId())!=null) {
			dao.updateDevice(device);
			return true;
		}
		return false;
	}

	@Override
	public Device getDevice(int deviceId) {
		return dao.getDevice(deviceId);
	}

	@Override
	public List<Device> getDevices() {
		return dao.getDevices();
	}

}
