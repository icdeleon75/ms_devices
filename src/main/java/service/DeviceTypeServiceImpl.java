package service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.DeviceTypeDao;
import model.DeviceType;

@Service
public class DeviceTypeServiceImpl implements DeviceTypeService{

	@Autowired
	DeviceTypeDao dao;
	
	@Override
	public boolean addDeviceType(DeviceType deviceType) {
		if(dao.getDeviceType(deviceType.getDeviceTypeId())==null){
			dao.addDeviceType(deviceType);
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteDeviceType(int deviceTypeId) {
		if(dao.getDeviceType(deviceTypeId)!=null){
			dao.deleteDeviceType(deviceTypeId);
			return true;
		}
		return false;
	}

	@Override
	public DeviceType getDeviceType(int deviceTypeId) {
		
		return dao.getDeviceType(deviceTypeId);
		
	}

}
