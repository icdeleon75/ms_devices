package service;

import java.util.List;

import model.Device;

public interface DeviceService {

	boolean addDevice(Device device);
	boolean deleteDevice(int deviceId);
	boolean updateDevice(Device device);
	Device getDevice(int deviceId);
	List<Device> getDevices();
	
}
