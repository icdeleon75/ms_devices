package service;

import model.DeviceType;

public interface DeviceTypeService {

	boolean addDeviceType(DeviceType deviceType);
	boolean deleteDeviceType(int deviceTypeId);
	DeviceType getDeviceType(int deviceTypeId);
}
