package dao;

import org.springframework.data.jpa.repository.JpaRepository;

import model.Device;

public interface DeviceJPA extends JpaRepository<Device, Integer> {

}
