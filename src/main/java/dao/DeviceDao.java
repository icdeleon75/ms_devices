package dao;

import java.util.List;

import model.Device;
public interface DeviceDao {

	 void addDevice(Device device);
	 void updateDevice(Device device);
	 void deleteDevice(int deviceId);
	 Device getDevice(int deviceId);
	 List<Device> getDevices();
}
