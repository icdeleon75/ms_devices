package dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import model.DeviceType;

@Repository
public class DeviceTypeImpl implements DeviceTypeDao{

	@Autowired
	DeviceTypeJPA jpa;
	
	@Override
	public void addDeviceType(DeviceType deviceType) {
		jpa.save(deviceType);
		
	}

	@Override
	public void deleteDeviceType(int deviceTypeId) {
		jpa.deleteById(deviceTypeId);
		
	}

	@Override
	public DeviceType getDeviceType(int deviceTypeId) {
		return jpa.findById(deviceTypeId).orElse(null);
	}

}
