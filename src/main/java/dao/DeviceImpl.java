package dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import model.Device;

@Repository
public class DeviceImpl implements DeviceDao {

	@Autowired
	DeviceJPA jpa;
	
	@Override
	public void addDevice(Device device) {
		jpa.save(device);

	}

	@Override
	public void updateDevice(Device device) {
		jpa.save(device);

	}

	@Override
	public void deleteDevice(int deviceId) {
		jpa.deleteById(deviceId);

	}

	@Override
	public Device getDevice(int deviceId) {
		return jpa.findById(deviceId).orElse(null);
	}

	@Override
	public List<Device> getDevices() {
		return jpa.findAll();
	}

}
