package dao;

import org.springframework.data.jpa.repository.JpaRepository;

import model.DeviceType;

public interface DeviceTypeJPA extends JpaRepository<DeviceType, Integer> {

}
