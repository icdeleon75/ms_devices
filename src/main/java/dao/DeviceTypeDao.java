package dao;

import model.DeviceType;

public interface DeviceTypeDao {
	
	 void addDeviceType(DeviceType deviceType);
	 void deleteDeviceType(int deviceTypeId);
	 DeviceType getDeviceType(int deviceTypeId);

}
